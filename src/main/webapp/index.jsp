<%-- 
    Document   : index
    Created on : 02-07-2021, 16:53:22
    Author     : Aorellana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Lista de links</h1>
        <h3>Heroku: https://solemne-eva3.herokuapp.com </h3><br>
        <br>
        <h2>Listar-(GET): https://solemne-eva3.herokuapp.com/api/empresas </h2><br>
        <h2>Listar por id(nombre)-(GET): https://solemne-eva3.herokuapp.com/api/empresas </h2><br>
        <h2>crear-(POST): https://solemne-eva3.herokuapp.com/api/empresas </h2><br>
        <h2>eliminar-(DELETE): https://solemne-eva3.herokuapp.com/api/empresas </h2><br>
        <h2>actualizar-(PUT): https://solemne-eva3.herokuapp.com/api/empresas </h2><br>
        <br>
        <h3>Bitbucket: https://bitbucket.org/sol-condor88/solemne-eva3/src </h3><br>
    </body>
</html>
