/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solemne3.services;

import com.mycompany.solemne3.dao.EmpresasJpaController;
import com.mycompany.solemne3.dao.exceptions.NonexistentEntityException;
import com.mycompany.solemne3.entity.Empresas;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Sorellana
 */
@Path("empresas")
public class EmpresasRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)

    public Response ListaEmpresas() {

        EmpresasJpaController dao = new EmpresasJpaController();
        List<Empresas> empresas = dao.findEmpresasEntities();

        return Response.ok(200).entity(empresas).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Empresas empresas) {

        EmpresasJpaController dao = new EmpresasJpaController();
        try {
            dao.create(empresas);
        } catch (Exception ex) {
            Logger.getLogger(EmpresasRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(empresas).build();

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Empresas empresas) {

        EmpresasJpaController dao = new EmpresasJpaController();

        try {
            dao.edit(empresas);
        } catch (Exception ex) {
            Logger.getLogger(EmpresasRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(empresas).build();

    }

    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("ideliminar") String ideliminar) {

        EmpresasJpaController dao = new EmpresasJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(EmpresasRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("Empresa eliminada").build();

    }

    @GET
    @Path("/{idConsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorId(@PathParam("idConsultar") String idConsultar) {

        EmpresasJpaController dao = new EmpresasJpaController();
        Empresas empresa = dao.findEmpresas(idConsultar);

        return Response.ok(200).entity(empresa).build();

    }
}
